# Ismani Nieuweboer - 10502815
# Lucas Slot        - 10610952
#
# Dubbele Bachelors Wiskunde & Informatica
#
# vehicle.py


from __future__ import print_function
from ast import literal_eval as read_tuple
import os

import numpy as np
import matplotlib.pyplot as plt


from vehicletypes import VehicleTypes

N_PARAMS = 21


class FileParser():
    def __init__(self, dirp, fp):
        self.dirp = dirp
        self.fp = fp

    def parse_metadata(self):
        self.sim_name = str(self.sim_data.readline()).strip()
        self.track_length = int(self.sim_data.readline().strip())
        self.n_lanes = int(self.sim_data.readline().strip())
        self.sim_data.readline()

        self.time_max = round(float(self.sim_data.readline().strip()), 1)
        self.time_step = round(float(self.sim_data.readline().strip()), 1)
        self.n_vehs = int(self.sim_data.readline().strip())
        self.sim_data.readline()

        # Cams
        self.cams = []
        line = self.sim_data.readline()
        while line.strip():
            self.cams.append(read_tuple(line))
            line = self.sim_data.readline()

    def parse_vehdata(self):
        # Skip avg, min, max vels
        for j in xrange(3*(self.n_lanes + 1)):
            self.sim_data.readline()

        # Vehicles
        vehicles_parsed = False
        self.line = self.sim_data.readline()

        while self.line.strip():
            vehicles_parsed = True
            veh_params = self.line.split('  ')

            veh_id = int(veh_params[4])

            self.veh_data[veh_id, 0, self.it] = float(veh_params[0])
            self.veh_data[veh_id, 1, self.it] = float(veh_params[1])
            self.veh_data[veh_id, 2, self.it] = int(veh_params[2])
            self.veh_data[veh_id, 3:20, self.it] = read_tuple(veh_params[3])
            self.veh_data[veh_id, 20, self.it] = float(veh_params[5])

            self.line = self.sim_data.readline()

        if vehicles_parsed:
            self.it += 1

        self.line = self.sim_data.readline()

    def parse_file(self):
        # Load the simulation and data needed for init.
        self.sim_data = open(self.dirp + self.fp, 'r')
        self.parse_metadata()

        # Amount of data points, one big array for all data
        self.n_data_points = self.time_max / self.time_step + 1
        self.veh_data = np.empty([self.n_vehs, N_PARAMS,  self.n_data_points])

        # Main loop
        self.it = 0
        # Read time
        self.line = self.sim_data.readline()
        while self.line.strip() != 'SIMULATION SUCCESFUL':
            self.parse_vehdata()


class SimPlotter():
    def __init__(self, dirp, printfp, mode=6):
        self.dirp = dirp
        self.printfp = printfp
        self.fileparsers = []
        self.mode = mode

    def parse_folder(self):
        count = 1
        totalcount = len(os.listdir(self.dirp))
        for fp in os.listdir(self.dirp):
            print("Checking file " + str(count) + " of " + str(totalcount))
            count += 1
            # If the file exists and is a log, parse it.
            if os.path.isfile(self.dirp + fp) and fp[0:3] == 'LOG':
                fileparser = FileParser(self.dirp, fp)
                fileparser.parse_file()
                self.fileparsers.append(fileparser)

    def create_fig(self):
        self.fig = plt.figure()
        self.fig.canvas.set_window_title('')

        self.ax = self.fig.add_subplot(111)

        if self.mode in [0, 2]:
            self.ax.set_title('Avgvel (m/s) as function of time')
        elif self.mode == 1:
            self.ax.set_title('Flow (vehs/s) as function of time')
        elif self.mode == 3:
            self.ax.set_title('Avg flow (vehs/s) as function of '
                              'vehicle density')
        elif self.mode == 4:
            self.ax.set_title('Avg flow (vehs/s) as function of speed limit')
        elif self.mode == 5:
            self.ax.set_title('Avg flow (vehs/s) as function of amount of '
                              'aggressive drivers (total 40)')
        elif self.mode == 6:
            self.ax.set_title('Avg energy/avg flow (vehs/s) as function of '
                              'vehicle density')

        if self.mode in [0, 1, 2]:
            self.ax.set_xlabel('Time')
        elif self.mode == 4:
            self.ax.set_xlabel('Speed limit')

    def plot_data(self):
        # Flow against time
        # Flow = n_vehs * avg_vel == sum(vels)
        # Assume that all simulations have the same timestep and are run
        # along the same time
        time_space = np.linspace(0, self.fileparsers[0].time_max,
                                 self.fileparsers[0].n_data_points)

        # Parameters are keys
        plotted_dict = {}

        for parser in self.fileparsers:
            # Avg vel
            if self.mode == 0:
                avg_vel = np.sum(parser.veh_data[:, 1, :], axis=0) /\
                    parser.n_vehs
                plotted = avg_vel
            # Flow
            elif self.mode == 1:
                flow = np.sum(parser.veh_data[:, 1, :], axis=0) /\
                    parser.track_length
                plotted = flow

            elif self.mode in [3, 6]:
                key = parser.n_vehs
            elif self.mode == 4:
                key = parser.cams[0][1]
            elif self.mode == 5:
                # Count aggressive drivers
                n_agg = 0
                for i in xrange(parser.n_vehs):
                    if int(parser.veh_data[i, 14, 0]) == \
                            VehicleTypes.Aggressive:
                        n_agg += 1
                print(n_agg)

                key = n_agg

            if self.mode in [0, 1]:
                self.ax.plot(time_space, plotted, label=str(parser.n_vehs))

            elif self.mode == 2:
                # Count aggressive drivers
                n_agg = 0
                for i in xrange(parser.n_vehs):
                    if int(parser.veh_data[i, 14, 0]) == \
                            VehicleTypes.Aggressive:
                        n_agg += 1
                self.ax.plot(time_space, plotted, label=str(n_agg))

            # Average flow
            elif self.mode in [3, 4, 5]:
                # Constant parameter over time, different across sims
                flow = np.sum(parser.veh_data[:, 1, :], axis=0) /\
                    parser.track_length
                avg_flow = np.sum(flow) / parser.n_data_points

                if key in plotted_dict:
                    plotted_dict[key].append(avg_flow)
                else:
                    plotted_dict[key] = [avg_flow]

            # Energy
            elif self.mode == 6:
                energy = np.sum(parser.veh_data[:, 20, :], axis=0) /\
                    parser.n_vehs
                avg_energy = np.sum(energy) / parser.n_data_points
                flow = np.sum(parser.veh_data[:, 1, :], axis=0) /\
                    parser.track_length
                avg_flow = np.sum(flow) / parser.n_data_points

                if key in plotted_dict:
                    plotted_dict[key].append(avg_energy / avg_flow)
                else:
                    plotted_dict[key] = [avg_energy / avg_flow]

        # Plot after iterating across parsers
        if self.mode in [3, 4, 5, 6]:
            param_space = []
            plotted = []

            # Combine results for each amount of vehicles
            for key, plot_list in plotted_dict.iteritems():
                param_space.append(key)
                # Average out for the amount of experiments done
                plotted.append(sum(plot_list) / len(plot_list))

            param_space, plotted = zip(*sorted(zip(param_space,
                                                   plotted),
                                               key=lambda t: t[0]))
            # Save results for future inspection
            with open(self.printfp, 'w+') as datafp:
                print(param_space, file=datafp)
                print(plotted, file=datafp)

            self.ax.plot(param_space, plotted)
            # self.ax.legend("Number of vehicles", "Flow (veh / s)")

    def set_legend(self):
        handles, labels = self.ax.get_legend_handles_labels()
        # sort both labels and handles by labels
        labels, handles = zip(*sorted(zip(labels, handles),
                                      key=lambda t: int(t[0])))
        labels = [n_vehs + ' aggressive of ' +
                  str(self.fileparsers[0].n_vehs)
                  for n_vehs in labels]
        self.ax.legend(handles, labels)

    def close_data(self):
        for parser in self.fileparsers:
            parser.sim_data.close()


def main(logdir, printfp):
    # Append slash if there isn't one
    if logdir[-1] != '/':
        logdir += '/'

    plotter = SimPlotter(logdir, printfp)
    plotter.parse_folder()
    plotter.create_fig()
    plotter.plot_data()
    # plotter.set_legend()

    plt.show()

    plotter.close_data()


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Create a plot.')
    parser.add_argument('logdir', type=str,
                        help='Directory of logs to create plot for')
    parser.add_argument('printfp', type=str,
                        help='File to write the plotting data to')
    args = parser.parse_args()

    main(args.logdir, args.printfp)
