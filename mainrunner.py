# -*- coding: utf-8 -*-
"""
Created on Fri Jan 29 13:41:29 2016

@author: Lucas
"""

import main
import os

for fn in os.listdir("."):
    if os.path.isfile(fn):
        main.main(fn, 200.0, 0.1)
