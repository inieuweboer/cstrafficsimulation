# Ismani Nieuweboer - 10502815
# Lucas Slot        - 10610952
#
# Dubbele Bachelors Wiskunde & Informatica
#
# vehiclegen.py

from numpy.random import normal
from vehicletypes import VehicleTypes


# Generates a car given a speed limit and a car type. Choices are
#   1. Normal driver.
#   2. Aggressive driver.
#   3. Passive driver.
#   4. Truck
# All parameters will be normally distributed.
# Returns a SIM-style string.
def generate_vehicle(speed_limit, pos, lane, vehicle_type, pol_mod):
    if vehicle_type == VehicleTypes.Normal:
        return generate_normal_car(speed_limit, pos, lane, pol_mod)
    if vehicle_type == VehicleTypes.Aggressive:
        return generate_agg_car(speed_limit, pos, lane, pol_mod)
    if vehicle_type == VehicleTypes.Passive:
        return generate_pass_car(speed_limit, pos, lane, pol_mod)
    if vehicle_type == VehicleTypes.Truck:
        return generate_truck(speed_limit, pos, lane, pol_mod)


def generate_normal_car(speed_limit, pos, lane, pol_mod):
    # Generate the vehicle parameters.
    delta = normal(4, 1)
    max_vel = normal(speed_limit * 0.9, speed_limit * 0.05)
    max_acc = normal(0.3, 0.01)
    max_dec = normal(4, 1)
    min_dist_ls = 10
    min_dist = 2
    time_head = normal(1.5, 0.3)

    length = normal(4.5, 0.5)
    front_area = normal(1.2, 0.3)
    mass = normal(1500, 300)
    rollres = 0.01
    vehicle_type = VehicleTypes.Normal

    ls_thresh = normal(0.1, 0.05)
    ls_bias = normal(0.2, 0.05)
    rtl_safe = normal(-4, 1)
    ltr_safe = normal(-0.2, 0.05)
    pol_fact = normal(1, 0.1) * pol_mod
    data = (round(delta, 2),
            round(max_vel, 2),
            round(max_acc, 2),
            round(max_dec, 2),
            round(min_dist_ls, 2),
            round(min_dist, 2),
            round(time_head, 2),
            round(length, 2),
            round(front_area, 2),
            round(mass, 2),
            round(rollres, 2),
            vehicle_type,
            round(ls_thresh, 2),
            round(ls_bias, 2),
            round(rtl_safe, 2),
            round(ltr_safe, 2),
            round(pol_fact, 2))

    # Set the starting speed as half the desired speed.
    vel = max_vel * 0.5

    return car_string(pos, vel, lane, data)


def generate_agg_car(speed_limit, pos, lane, pol_mod):
    # Generate the vehicle parameters.
    delta = normal(10, 2.5)
    max_vel = normal(speed_limit * 1.1, speed_limit * 0.05)
    max_acc = normal(2, 0.3)
    max_dec = normal(5, 1)
    min_dist_ls = 10
    min_dist = 2
    time_head = normal(1.2, 0.3)

    length = normal(4.5, 0.5)
    front_area = normal(1.2, 0.3)
    mass = normal(1500, 300)
    rollres = 0.01
    vehicle_type = VehicleTypes.Aggressive

    ls_thresh = normal(0.08, 0.05)
    ls_bias = normal(0.1, 0.05)
    rtl_safe = normal(-6, 1)
    ltr_safe = normal(-4, 0.5)
    pol_fact = normal(0.5, 0.1) * pol_mod
    data = (round(delta, 2),
            round(max_vel, 2),
            round(max_acc, 2),
            round(max_dec, 2),
            round(min_dist_ls, 2),
            round(min_dist, 2),
            round(time_head, 2),
            round(length, 2),
            round(front_area, 2),
            round(mass, 2),
            round(rollres, 2),
            vehicle_type,
            round(ls_thresh, 2),
            round(ls_bias, 2),
            round(rtl_safe, 2),
            round(ltr_safe, 2),
            round(pol_fact, 2))

    # Set the starting speed as half the desired speed.
    vel = max_vel * 0.5

    return car_string(pos, vel, lane, data)


def generate_pass_car(speed_limit, pos, lane, pol_mod):
    # Generate the vehicle parameters.
    delta = normal(2, 0.3)
    max_vel = normal(speed_limit * 0.75, speed_limit * 0.05)
    max_acc = normal(0.3, 0.01)
    max_dec = normal(3, 0.75)
    min_dist_ls = 10
    min_dist = 2
    time_head = normal(2, 0.4)

    length = normal(4.5, 0.5)
    front_area = normal(1.2, 0.3)
    mass = normal(1500, 300)
    rollres = 0.01
    vehicle_type = VehicleTypes.Passive

    ls_thresh = normal(0.15, 0.05)
    ls_bias = normal(0.3, 0.05)
    rtl_safe = normal(-1, 0.4)
    ltr_safe = normal(-0.2, 0.05)
    pol_fact = normal(2, 0.2) * pol_mod
    data = (round(delta, 2),
            round(max_vel, 2),
            round(max_acc, 2),
            round(max_dec, 2),
            round(min_dist_ls, 2),
            round(min_dist, 2),
            round(time_head, 2),
            round(length, 2),
            round(front_area, 2),
            round(mass, 2),
            round(rollres, 2),
            vehicle_type,
            round(ls_thresh, 2),
            round(ls_bias, 2),
            round(rtl_safe, 2),
            round(ltr_safe, 2),
            round(pol_fact, 2))

    # Set the starting speed as half the desired speed.
    vel = max_vel * 0.5

    return car_string(pos, vel, lane, data)


def generate_truck(speed_limit, pos, lane, pol_mod):
    # Generate the vehicle parameters.
    delta = normal(4, 1)
    max_vel = normal(speed_limit * 0.60, speed_limit * 0.05)
    max_acc = normal(0.2, 0.005)
    max_dec = normal(3, 0.75)
    min_dist_ls = 20
    min_dist = 4
    time_head = normal(1.5, 0.3)

    length = normal(12, 0.5)
    front_area = normal(6, 0.5)
    mass = normal(3000, 500)
    rollres = 0.01
    vehicle_type = VehicleTypes.Truck

    ls_thresh = normal(0.2, 0.05)
    ls_bias = normal(0.3, 0.05)
    rtl_safe = normal(-4, 1)
    ltr_safe = normal(-0.2, 0.05)
    pol_fact = normal(1, 0.1) * pol_mod
    data = (round(delta, 2),
            round(max_vel, 2),
            round(max_acc, 2),
            round(max_dec, 2),
            round(min_dist_ls, 2),
            round(min_dist, 2),
            round(time_head, 2),
            round(length, 2),
            round(front_area, 2),
            round(mass, 2),
            round(rollres, 2),
            vehicle_type,
            round(ls_thresh, 2),
            round(ls_bias, 2),
            round(rtl_safe, 2),
            round(ltr_safe, 2),
            round(pol_fact, 2))

    # Set the starting speed as half the desired speed.
    vel = max_vel * 0.5

    return car_string(pos, vel, lane, data)


def car_string(pos, vel, lane, data):
    return str(pos) + "  " + str(vel) + "  " + str(lane) + "  " + str(data)
