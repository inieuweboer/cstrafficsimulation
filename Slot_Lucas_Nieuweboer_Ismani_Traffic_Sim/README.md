# comp_sci_traffic_sim
Project Computational Science - Traffic Simulation

Python implementation:
Our implementation in python is in the main folder. The key files are:
main.py
simulation.py
lane.py
vehicle.py
vehicletypes.py

A simulation can be run by feeding a proper SIM-file to main.py:
	main.py SIM_example_simulation



For animation of LOG-files, the following files /folders are used:
animation.py
primitives.py (this file was not written by us, proper reference is present in the file)
sprites (a folder containing all sprites used)

To animate a LOG:
	animation.py LOG_example_log

Note that for simulations with a particularly long track length, the cars might
scale to such a small size that they can't be seen anymore.



Finally, the following files were used by us to generate simulations and analyze LOG-files:
simulationgen.py
vehiclegen.py
plotting.py

These files are not intended to be used or reviewed.



The APPENDICES folder contains several files referenced to in the report
The IMAGES folder contains several images used in the paper (in larger resolution)
the EXPERIMENTS folder contains all relevant SIM-files, as well as a few LOG files that
are referenced to in the report. We decided not to include all LOG files as this made the
folder too big.
