# Ismani Nieuweboer - 10502815
# Lucas Slot        - 10610952
#
# Dubbele Bachelors Wiskunde & Informatica
#
# vehicle.py
#
# Contains the vehicle class, which contains all logic and functions needed
# to simulate the behaviour of a single vehicle-agent within a simulation,
# including static functions used to implement the IDM.

import numpy as np

# MOBIL parameters.
CONGESTION_VEL = 5.
ANNOYANCE_TIME = 60.
ANNOYANCE_MAX = 0.5
ANNOY_ON = 1.0  # 1: on, 0: off.

# Traffic cam parameters.
TRAFFIC_CAM_DELTA = 10
TRAFFIC_CAM_DISTANCE = 50.

# Energy consumption parameters.
DENSITY = 1.3
GRAV = 9.8


# A vehicle is initialized using 4 arguments:
#   pos:    The position of the vehicle.
#   vel:    The velocity of the vehicle.
#   lane:   The lane in which the vehicle drives.
#   name:   A unique ID for the vehicle.
#   data:   A tuple containing information about the
#           vehicle's specifications and the driver's tendencies,
#           consisting, in order, of:
#               delta:      The aggressiveness of the driver.
#               max_vel:    The driver's desired velocity
#               max_acc:    The driver's maximal desired acceleration
#               max_dec:    The driver's maximal comfortable deceleration.
#               min_dist_ls:The minimal distance (m) the driver wishes
#                           to keep from his successor when lane-swapping.
#               min_dist:   The minimal distance (m) the driver wishes
#                           to keep from his predecessor.
#               time_head:  The time-distance (s) the drivers wishes to keep
#                           to keep from his predecessor.
#               length:     The vehicles length.
#               front area: The area of the vehicles front (m^2)
#               mass:       The mass of the vehicle (kg)
#               rollres   The vehicle's friction (Ns/m)
#               veh_type:   The vehicle's type (e.g. Car, Truck),
#                           used for animation
#               ls_thresh:  The driver's lane-swapping threshold.
#               ls_bias:    The driver's tendency to keep right.
#               rtl_safe:   The minimal acceleration the driver wishes to put
#                           his new successor under after lane-swapping (RTL).
#               ltr_safe    The minimal acceleration the driver wishes to put
#                           his new successor under after lane-swapping (LTR).
#               pol_fact:   The driver's politeness level.
class Vehicle:
    def __init__(self, pos, vel, lane, name, data):
        (self.delta,
         self.max_vel,
         self.max_acc,
         self.max_dec,
         self.min_dist,
         self.min_dist_ls,
         self.time_head,

         self.length,
         self.front_area,
         self.mass,
         self.rollres,
         self.veh_type,

         self.ls_thresh,
         self.ls_bias,
         self.rtl_safe,
         self.ltr_safe,
         self.pol_fact) = self.data = data
        self.pos = pos
        self.vel = vel
        self.lane = lane

        self.name = name

        self.annoyance = 0
        self.work = 0

        # These -should- be reset by the simulator.
        self.n_lanes = 1
        self.track_length = -1

    @staticmethod
    def find_acc(veh, predec, cur_vel):
        """
        Finds the acceleration of a vehicle given a predecessor and current
        velocity according to the IDM.
        """

        # Calculate the space to the predecessor, if there is one.
        if veh.name == predec.name:
            space = veh.max_vel * veh.time_head * 100
        else:
            space = (predec.pos - veh.pos) % veh.track_length - \
                predec.length

        # Space can be negative (when comparing to a car in another lane).
        # Make it very small instead.
        if space < 0:
            space = 0.1

        # Compensate if there is a traffic cam.
        max_vel, delta = veh.traffic_cam_adjust()

        # Get the model help-variables.
        delta_vel = cur_vel - predec.vel
        s_star = veh.min_dist + cur_vel * veh.time_head + \
            cur_vel * delta_vel / (2 * np.sqrt(veh.max_acc * veh.max_dec))

        # s_star should never be negative.
        if s_star < 0:
            s_star = 0

        # Get the acceleration from the model.
        vel_dot = veh.max_acc * (1 - (cur_vel / max_vel)**delta -
                                 (s_star*s_star / (space*space)))
        return vel_dot

    @staticmethod
    def derivs_pos(y, t, *args):
        """
        Derivative function for the IDM differential equation, with argument
        vehicle velocity y.
        """
        veh = args[0]
        predec = args[1]
        vel_dot = Vehicle.find_acc(veh, predec, y[1])
        pos_dot = y[1]

        return [pos_dot, vel_dot]

    @staticmethod
    def dist(pos1, pos2, track_length):
        """
        Find the distance between two positions modulo the track length.
        """
        dist1 = (pos1 - pos2) % track_length
        dist2 = (pos2 - pos1) % track_length
        return min(dist1, dist2)

    @staticmethod
    def power_drag(vel, area, density=DENSITY):
        """
        Find the power used due to the air drag on the vehicle
        """
        return vel**3 * area * density / 2.0

    @staticmethod
    def power_tires(vel, mass, rollres, grav=GRAV):
        """
        Find the power used due to the road friction.
        """
        return vel * mass * rollres * grav

    @staticmethod
    def energy_vel_diff(vel_0, vel_t, mass, time_step):
        """
        Find the energy used due to the velocity difference
        """
        return mass * (vel_t*vel_t - vel_0*vel_0) / 2

    def calc_power(self, density=DENSITY, grav=GRAV):
        """
        Calculate current power usage.
        """
        return Vehicle.power_drag(self.vel, self.front_area, density) + \
            Vehicle.power_tires(self.vel, self.mass, self.rollres, grav)

    def update_work(self, time_step, density=DENSITY, grav=GRAV):
        """
        Increase total work done based on current power usage,
        and vehicle acceleration.
        """
        self.work += self.calc_power(density, grav) * time_step + \
            Vehicle.energy_vel_diff(self.vel, self.next_vel, self.mass,
                                    time_step)

    def traffic_cam_adjust(self):
        """
        Check if there is a nearby traffic cam and return adjusted max speed.
        """
        new_vel = self.max_vel
        delta = self.delta

        for pos in self.traffic_cams:
            dist = Vehicle.dist(self.pos, pos, self.track_length)
            cam_vel = self.traffic_cams[pos].max_vel

            if dist < TRAFFIC_CAM_DISTANCE and self.vel > cam_vel:
                new_vel = cam_vel
                delta = max(self.delta, TRAFFIC_CAM_DELTA)

        return new_vel, delta

    def calc_vals(self, time_step):
        """
        Calculate new velocity and speed.
        """
        self.next_vel = self.vel + Vehicle.find_acc(self,
                                                    self.predec,
                                                    self.vel) * time_step
        # Never move backwards.
        self.next_vel = max(self.next_vel, 0)
        self.next_pos = self.pos + self.vel * time_step

    def update_vel(self):
        """
        Set the new velocity.
        """
        self.vel = self.next_vel

    def update_pos(self, time_step):
        """
        Set the new position.
        """
        self.pos = self.next_pos % self.track_length
        self.check_lanechange(time_step)

    def check_lanechange(self, time_step):
        """
        Check whether the vehicle should change to another lane. Perform the
        the change if so.
        """
        # Get the incentive to move right and left.
        ltr_inc, ltr_brake, new_predec_right, ltr_dist_suc, ltr_dist_prec = \
            self.to_right_inc(time_step)
        rtl_inc, rtl_brake, new_predec_left, rtl_dist_suc, rtl_dist_prec =  \
            self.to_left_inc(time_step)

        # Check if moving to another lane is wanted and safe.
        if rtl_inc > self.ls_thresh and \
                rtl_brake > self.rtl_safe and \
                rtl_dist_suc > self.min_dist_ls and \
                rtl_dist_prec > self.min_dist_ls:

            self.change_lane(self.lane.lane_l, new_predec_left)

        elif ltr_inc > self.ls_thresh and \
                ltr_brake > self.ltr_safe and \
                ltr_dist_suc > self.min_dist_ls and \
                ltr_dist_prec > self.min_dist_ls:
            self.change_lane(self.lane.lane_r, new_predec_right)

    def to_left_inc(self, time_step):
        """
        Get the incentive and safety variables for a left lane-swap.
        """
        # Check if lane to left exists.
        if self.lane.lane_l is None:
            return 0, 0, None, 0, 0

        # Find a_c^eur.
        a_c = Vehicle.find_acc(self, self.predec, self.vel)

        # Find tilde(a)_c and the politeness term.
        new_predec = self.lane.lane_l.find_predec(self)
        # Lane to left is empty:
        if new_predec is None:
            ta_c = Vehicle.find_acc(self, self, self.vel)
            rtl_acc = 0
            pol_term = 0
            dist_suc = dist_prec = self.min_dist + 1

        # Lane to left is not empty:
        else:
            new_succes = new_predec.succes
            ta_c = Vehicle.find_acc(self, new_predec, self.vel)

            # European passing rules.
            if self.vel > new_predec.vel > CONGESTION_VEL:
                a_c = min(ta_c, a_c)

            rtl_acc = Vehicle.find_acc(new_succes, self, new_succes.vel)
            pol_term = (self.pol_fact - (self.annoyance * ANNOY_ON)) * \
                (rtl_acc -
                 Vehicle.find_acc(new_succes, new_predec, new_succes.vel))

            dist_suc = Vehicle.dist(self.pos,
                                    new_succes.pos,
                                    self.track_length)
            dist_prec = Vehicle.dist(self.pos,
                                     new_predec.pos,
                                     self.track_length)

        # If there is incentive to go left, but the politeness blocks this,
        # increase annoyance.
        if ta_c - a_c > self.ls_bias and \
                self.annoyance < ANNOYANCE_MAX * self.pol_fact:
            self.annoyance += self.pol_fact * time_step / ANNOYANCE_TIME

        # Compute and return final incentive.
        rtl_inc = ta_c - a_c + pol_term - self.ls_bias
        return rtl_inc, rtl_acc, new_predec, dist_suc, dist_prec

    def to_right_inc(self, time_step):
        """
        Get the incentive and safety variables for a left lane-swap.
        """

        # Check if lane to right exists.
        if self.lane.lane_r is None:
            return 0, 0, None, 0, 0

        # Find a_c.
        a_c = Vehicle.find_acc(self, self.predec, self.vel)

        # Find tilde(a)_c^eu.
        new_predec = self.lane.lane_r.find_predec(self)
        # Lane to right is empty:
        if new_predec is None:
            ta_c = Vehicle.find_acc(self, self, self.vel)
            ltr_acc = 0
            dist_suc = dist_prec = self.min_dist + 1
        # Lane to right is not empty
        else:
            new_succes = new_predec.succes
            ta_c = Vehicle.find_acc(self, new_predec, self.vel)
            ltr_acc = Vehicle.find_acc(new_succes, self, new_succes.vel)
            dist_suc = Vehicle.dist(self.pos,
                                    new_succes.pos,
                                    self.track_length)
            dist_prec = Vehicle.dist(self.pos,
                                     new_succes.pos,
                                     self.track_length)

        # European passing rule.
        if self.predec.vel > CONGESTION_VEL:
            ta_c = min(ta_c, a_c)

        # Find the politeness term.
        # No one on own lane:
        if len(self.lane.vehicles) == 1:
            pol_term = 0
        # Someone else on own lane:
        else:
            pol_term = self.pol_fact * \
                (Vehicle.find_acc(self.succes, self.predec, self.succes.vel) -
                 Vehicle.find_acc(self.succes, self, self.succes.vel))

        # Compute and return final incentive.
        ltr_inc = ta_c - a_c + pol_term + self.ls_bias

        return ltr_inc, ltr_acc, new_predec, dist_suc, dist_prec

    def change_lane(self, new_lane, new_predec):
        """
        Perform a lane change by adding / removing vehicles to / from lanes.
        """

        # Reset annoyance
        self.annoyance = 0

        # Fix linked list
        self.succes.predec = self.predec
        self.predec.succes = self.succes

        # Set new predec and succes
        if new_predec is None:
            self.predec = self
            self.succes = self
        else:
            self.predec = new_predec
            self.succes = self.predec.succes
            self.succes.predec = self
            self.predec.succes = self

        # Remove from previous and add to new vehicle dict.
        new_lane.vehicles[self.name] = self.lane.vehicles.pop(self.name)
        self.lane = new_lane

    def log_entry(self):
        """
        Returns the line that should be logged for this vehicle.
        """
        return str(self.pos) + "  " + str(self.vel) + "  " + str(self.lane) + \
            "  " + str(self.data) + "  " + str(self.name) + \
            "  " + str(self.calc_power())

    def __repr__(self):
        return (str(self.name) + " is driving at " + str(self.pos) +
                " going " + str(self.vel) + " m/s.")

    # Define functions used to sort by position.
    def __lt__(self, other):
        return self.pos < other.pos

    def __gt__(self, other):
        return self.pos > other.pos

    def __eq__(self, other):
        return self.pos == other.pos

    def __le__(self, other):
        return self.pos <= other.pos

    def __ge__(self, other):
        return self.pos >= other.pos

    def __ne__(self, other):
        return self.pos != other.pos
