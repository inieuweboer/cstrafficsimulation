# Ismani Nieuweboer - 10502815
# Lucas Slot        - 10610952
#
# Dubbele Bachelors Wiskunde & Informatica
#
# simulation.py
#
# Contains the logic and overhead for starting up, performing and ending a
# simulation. Contains all lanes, which in turn contain vehicles.


from __future__ import print_function
from ast import literal_eval as read_tuple

from lane import Lane
from vehicle import Vehicle


class Simulator:
    def __init__(self):
        self.time = 0
        self.lanes = []
        self.assign = Counter()
        self.progress = 0

    def end_simulation(self, succes):
        """
        End the simulation, do cleanup.
        """
        self.time = 0
        self.lanes = []
        self.progress = 0

        if succes:
            print("SIMULATION SUCCESFUL", file=self.log)
        else:
            print("SIMULATION FAILED", file=self.log)

        self.log.close()
        print("Logfile closed.")
        print("Simulation ended.")

    def min_vel(self):
        """
        Find the minimum velocity of all vehicles.
        """
        # Assume there is at least one lane.
        if len(self.lanes[0].vehicles) == 0:
            return 0

        min_vel = next(self.lanes[0].vehicles.itervalues()).vel

        for lane in self.lanes:
            min_vel = min(min_vel, lane.min_vel())

        return min_vel

    def max_vel(self):
        """
        Find the maximum velocity of all vehicles.
        """
        # Assume there is at least one lane.
        if len(self.lanes[0].vehicles) == 0:
            return 0

        max_vel = next(self.lanes[0].vehicles.itervalues()).vel

        for lane in self.lanes:
            max_vel = max(max_vel, lane.max_vel())

        return max_vel

    def avg_vel(self):
        """
        Find the average velocity of all vehicles.
        """
        tot = 0.0

        for lane in self.lanes:
            n_lane_vehs = len(lane.vehicles)
            tot += n_lane_vehs * lane.avg_vel()

        return tot / self.n_vehs

    def update_log(self):
        """
        Log the current simulation state.
        """
        # Print time.
        print(str(self.time), file=self.log)

        # Print global info.
        print(str(self.avg_vel()), file=self.log)
        print(str(self.min_vel()), file=self.log)
        print(str(self.max_vel()), file=self.log)

        # Print info per lane.
        for lane in self.lanes:
            print(str(lane.num) + '  ' + str(lane.avg_vel()), file=self.log)
            print(str(lane.num) + '  ' + str(lane.min_vel()), file=self.log)
            print(str(lane.num) + '  ' + str(lane.max_vel()), file=self.log)

        # Print vehicle info.
        for lane in self.lanes:
            for veh in lane.vehicles.itervalues():
                print(veh.log_entry(), file=self.log)

        print("", file=self.log)

    def update(self, time_step):
        """
        Update the simulation by one time step.
        """
        # First calculate new vel and pos values.
        for lane in self.lanes:
            for veh in lane.vehicles.itervalues():
                veh.calc_vals(time_step)
                veh.update_work(time_step)

        # Then apply them.
        for lane in self.lanes:
            for key in lane.vehicles.keys():
                veh = lane.vehicles[key]
                veh.update_vel()
                veh.update_pos(time_step)

        # Increase time and show progress.
        self.time += time_step
        if self.time > (self.time_max / 100.) * self.progress:
            print(str(self.progress) + "%")
            self.progress += 1

    def show_state(self):
        """
        Print location and velocity for all vehicles.
        """
        print("Time is: " + str(self.time))
        print("Average velocity is: " + str(self.avg_vel()))

        for lane in self.lanes:
            for veh in lane.vehicles.itervalues():
                print(veh)

        print

    def init_lanes(self):
        """
        Create all lane objects.
        """
        for i in xrange(self.n_lanes):
            lane = Lane(num=i,
                        track_length=self.track_length,
                        t_cams=self.traffic_cams)
            self.lanes.append(lane)

        # Set neighbouring lanes. Rightmost lane is the zeroth
        for i in xrange(self.n_lanes - 1):
            self.lanes[i].lane_l = self.lanes[i+1]
            self.lanes[i+1].lane_r = self.lanes[i]

    def init_vehicles(self, simdata):
        """
        Create all vehicle objects and add them to the lanes.
        """
        self.n_vehs = 0

        for line in simdata:
            # Ignore commented lines
            if line[0] == '#':
                continue

            veh_params = line.split("  ")

            veh_pos = float(veh_params[0].strip())
            veh_vel = float(veh_params[1].strip())
            veh_lane = int(veh_params[2].strip())
            veh_data = read_tuple(veh_params[3])

            new_veh = Vehicle(veh_pos,
                              veh_vel,
                              self.lanes[veh_lane],
                              self.assign.get_number(),
                              veh_data)
            self.lanes[veh_lane].add_vehicle(new_veh)

            self.n_vehs += 1

    def init_traffic_cams(self, simdata):
        """
        Create all traffic cam objects.
        """
        self.traffic_cams = {}
        line = simdata.readline().strip()

        while line:
            info = read_tuple(line)
            pos, max_vel = int(info[0]), int(info[1])
            new_cam = TrafficCamera(pos, max_vel)
            self.traffic_cams[pos] = new_cam
            line = simdata.readline().strip()

        print(self.traffic_cams)

    def load_sim(self, simfp):
        """
        Load a simulation from a file.
        """
        with open(simfp, 'r') as simdata:
            self.logfp = "LOG_" + str(simdata.readline()).strip()
            self.track_length = int(simdata.readline().strip())

            # Skip over a blank line.
            simdata.readline()

            # Initialize the traffic cams.
            self.init_traffic_cams(simdata)

            # Initialize the lanes.
            self.n_lanes = int(simdata.readline().strip())
            if self.n_lanes < 1:
                raise ValueError('Amount of lanes should be at least 1')
            self.init_lanes()

            # Skip over a blank line.
            simdata.readline()

            # Initialize the vehicles.
            self.init_vehicles(simdata)

        # Set the predecessors for each lane and vehicle.
        for lane in self.lanes:
            lane.set_predecs_and_succsecs()

    def start_log(self):
        """
        Start the log file.
        """
        self.log = open(self.logfp, "w+")

        print(self.logfp, file=self.log)
        print(self.track_length, file=self.log)
        print(len(self.lanes), file=self.log)

        print("", file=self.log)

        print(str(self.time_max), file=self.log)
        print(str(self.time_step), file=self.log)
        print(str(self.n_vehs), file=self.log)

        print("", file=self.log)

        for cam in self.traffic_cams.itervalues():
            print(cam, file=self.log)

        print("", file=self.log)

    def run(self, time_step, time_max, simfp, verbose=False, log=True):
        """
        Run the simulation.
        """

        self.time_step = time_step
        self.time_max = time_max

        # Load the simulation.
        self.load_sim(simfp)

        # Start the log.
        if log:
            self.start_log()

        # Main loop.
        while self.time < time_max:
            if verbose:
                self.show_state()

            if log:
                self.update_log()

            self.update(time_step)

        self.end_simulation(True)


class TrafficCamera():
    def __init__(self, pos, max_vel):
        self.pos = pos
        self.max_vel = max_vel

    def __repr__(self):
        return str((self.pos, self.max_vel))


# Used to give vehicles unique IDs.
class Counter():
    def __init__(self):
        self.counter = 0

    def get_number(self):
        counter = self.counter
        self.counter += 1
        return counter
