# Ismani Nieuweboer - 10502815
# Lucas Slot        - 10610952
#
# Dubbele Bachelors Wiskunde & Informatica
#
# main.py
#
# Performs a simulation given a SIM-file, duration and time-step.


from simulation import Simulator


def main(simfile, duration, time_step):
    simulator = Simulator()
    print(time_step)
    print(duration)
    simulator.run(time_step, duration, simfile, verbose=False)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Run a simulation.')
    parser.add_argument('simfile',
                        type=str,
                        help='filename of simulation file')
    parser.add_argument('duration',
                        type=float,
                        const=200.0,
                        default=200.0,
                        nargs='?',
                        help='duration of the simulation (s)')
    parser.add_argument('time_step',
                        type=float,
                        const=0.1,
                        default=0.1,
                        nargs='?',
                        help='time-step used in simulation (s)')
    args = parser.parse_args()

    main(args.simfile, args.duration, args.time_step)
