# Ismani Nieuweboer - 10502815
# Lucas Slot        - 10610952
#
# Dubbele Bachelors Wiskunde & Informatica
#
# lane.py
#
# Contains logic and overhead for a single lane, inclusing a dictionary of
# all vehicles in the lane.


class Lane:
    def __init__(self, num, track_length, t_cams, lane_l=None, lane_r=None):
        # Identifier for lane
        self.num = num

        self.track_length = track_length

        self.lane_l = lane_l
        self.lane_r = lane_r
        self.vehicles = {}
        self.traffic_cams = t_cams

    def add_vehicle(self, veh):
        """
        Add a vehicle to the lane.
        """
        self.vehicles[int(veh.name)] = veh
        veh.track_length = self.track_length
        veh.traffic_cams = self.traffic_cams

    def set_predecs_and_succsecs(self):
        """
        Find the successor and predecessor for each car.
        """
        n = len(self.vehicles)

        # Get the keys in order of corresponding vehicle position.
        key_order = sorted(self.vehicles, key=self.vehicles.get)

        # Assign pre/suc-cessors per key
        for i in xrange(n):
            self.vehicles[key_order[i]].predec = \
                self.vehicles[key_order[(i+1) % n]]
            self.vehicles[key_order[i]].succes = \
                self.vehicles[key_order[(i-1) % n]]

    def find_predec(self, cur_veh):
        """
        Find the predecessor for a vehicle by looking up the closest other
        vehicle within the lane.
        """
        # TODO gebruik linked list voor betere best case
        new_dist = self.track_length + 1

        predec = None

        for lane_veh in self.vehicles.itervalues():
            # Distance from current vehicle to vehicle from other lane
            dist = (lane_veh.pos - cur_veh.pos) % self.track_length

            if dist < new_dist:
                new_dist = dist
                predec = lane_veh

        return predec

    def min_vel(self):
        """
        Find the minimum velocity of all vehicles in this lane.
        """
        if len(self.vehicles) == 0:
            return 0

        min_vel = next(self.vehicles.itervalues()).vel

        for veh in self.vehicles.itervalues():
            min_vel = min(min_vel, veh.vel)

        return min_vel

    def max_vel(self):
        """
        Find the maximum velocity of all vehicles in this lane.
        """
        if len(self.vehicles) == 0:
            return 0

        max_vel = next(self.vehicles.itervalues()).vel

        for veh in self.vehicles.itervalues():
            max_vel = max(max_vel, veh.vel)

        return max_vel

    def avg_vel(self):
        """
        Find the average velocity of all vehicles in this lane.
        """
        tot = 0.0

        for veh in self.vehicles.itervalues():
            tot += veh.vel

        n_vehs = len(self.vehicles)

        if n_vehs == 0:
            return 0
        else:
            return tot / float(n_vehs)

    def __repr__(self):
        return str(self.num)
