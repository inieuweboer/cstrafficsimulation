# Ismani Nieuweboer - 10502815
# Lucas Slot        - 10610952
#
# Dubbele Bachelors Wiskunde & Informatica
#
# simulationgen.py


from __future__ import print_function
from vehiclegen import generate_vehicle
from vehicletypes import VehicleTypes
import sys as system

VEH_SPACING = 20


def generate_simulation(name, speed_limit, track_length, cams, lanes, veh_amts,
                        spacing=-1, pol_mod=1):
    # Create the SIM-file.
    sim = open("SIM_" + name, 'w')

    # Print the name, track length and amount of lanes.
    start_simulation(sim, name, track_length, cams, lanes)

    # Default equally spaced
    if spacing == -1:
        spacing = float(track_length) / sum(veh_amts) - 0.01

    # Distribute the vehicles evenly over the lanes, starting at lane 0 on pos
    # 0, and placing them 20 meters apart.
    cur_lane = cur_pos = 0
    total_amt = sum(veh_amts)
    for i in xrange(total_amt):
        # Create normal cars.
        if i < veh_amts[0]:
            print(generate_vehicle(speed_limit, cur_pos, cur_lane,
                                   VehicleTypes.Normal, pol_mod), file=sim)
        # Create aggressive cars.
        elif i < veh_amts[0] + veh_amts[1]:
            print(generate_vehicle(speed_limit, cur_pos, cur_lane,
                                   VehicleTypes.Aggressive, pol_mod), file=sim)
        # Create passive cars.
        elif i < veh_amts[0] + veh_amts[1] + veh_amts[2]:
            print(generate_vehicle(speed_limit, cur_pos, cur_lane,
                                   VehicleTypes.Passive, pol_mod), file=sim)
        # Create truck.
        else:
            print(generate_vehicle(speed_limit, cur_pos, cur_lane,
                                   VehicleTypes.Truck, pol_mod), file=sim)

        cur_pos += spacing
        if cur_pos > track_length:
            cur_pos = 0
            cur_lane = cur_lane + 1
            if cur_lane > lanes - 1:
                system.exit("Cannot fit all vehicles!")


def generate_simulation2(name, speed_limit, track_length, lanes, vehs):
    # Create the SIM-file.
    sim = open("SIM_" + name, 'w')

    # Print the name, track length and amount of lanes.
    start_simulation(sim, name, track_length, lanes)

    for veh in vehs:
        print(generate_vehicle(speed_limit,
                               veh[0],
                               veh[1],
                               veh[2]), file=sim)


def start_simulation(sim, name, track_length, cams, lanes):
    # Print the name, track length and amount of lanes.
    print(name, file=sim)
    print(track_length, file=sim)
    print("", file=sim)

    for cam in cams:
        print(cam, file=sim)

    print("", file=sim)
    print(lanes, file=sim)
    print("", file=sim)


def main(simfile):
    for i in xrange(5):
        # ======= MOBIL
        # for j in [-1.0, 0.0, 0.5, 1.0, 1.5, 2.0]:
        #     generate_simulation("MOBIL_pol_fact_" + str(j) +
        #                         "(" + str(i + 1) + ")",
        #                         30, 500, [], 4, [14, 2, 2, 2], pol_mod=j)
        # for j in [-0.5, -0.25, 0.25]:
        #     generate_simulation("MOBIL_pol_fact_" + str(j) +
        #                         "(" + str(i + 1) + ")",
        #                         30, 500, [], 4, [14, 2, 2, 2], pol_mod=j)

        # ======= Jams
        # for j in [10,20,30,32,34,36,38,40,50,60,70]:
        #     generate_simulation("single_lane_jame_" + str(j) +
        #                         "(" + str(i + 1) + ")",
        #                         30, 2000, 1, [j, 0, 0, 0])

        # ======= Traffic cams
        # for j in [10, 14, 18, 22, 28, 32]:
        #     generate_simulation(simfile + "_" + str(j) +
        #                         "(" + str(i + 1) + ")",
        #                         30, 1000, [(100, j)], 4, [28, 4, 4, 4])

        # ======= Agg drivers
        # for j in [10, 15, 20, 25, 30]:
        #     generate_simulation(simfile + "_" + str(j) +
        #                         "(" + str(i + 1) + ")",
        #                         30, 1000, [], 4, [40 - j, j, 0, 0])

        # ======= Energy usage
        for j in [2, 3, 4, 6, 8]:
            generate_simulation(simfile + "_" + str(j) +
                                "(" + str(i + 1) + ")",
                                30, 1000, [], 4, [7*j, j, j, j])


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Generate a simulation.')
    parser.add_argument('simfile', type=str,
                        help='filename for simulation file to be created')
    args = parser.parse_args()

    main(args.simfile)
