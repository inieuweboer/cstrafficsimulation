# Ismani Nieuweboer - 10502815
# Lucas Slot        - 10610952
#
# Dubbele Bachelors Wiskunde & Informatica
#
# animation.py
#
# Animates a simulation from its LOG-file.


import pyglet
from ast import literal_eval as read_tuple
import primitives as pt
import sys

from vehicletypes import VehicleTypes

# Constants for animation.
FPS = 40.0
SIMPLE_CAR_SPRITE = pyglet.image.load('sprites/car.png')
SIMPLE_TRUCK_SPRITE = pyglet.image.load('sprites/truck.png')
ROAD_BG = pyglet.image.load('sprites/road.png')

LANE_WIDTH = 30
LANE_STROKE = 2
MULT = 2.2
VEHICLE_WIDTH = 2

WINDOW_WIDTH = 1980

MARKER_LINE_HEIGHT = 35
SPACER_HEIGHT = 50

TEXTBOX_WIDTH = 120
TEXT_LINE_HEIGHT = 35
TEXTBOX_X_OFFSET = 10
TEXTBOX_Y_OFFSET = 10


class VehicleSprite(pyglet.sprite.Sprite):
    def __init__(self, pos, y_offset, vel, params, batch, group, lane=0,
                 scale=1):
        self.rescale = scale  # This is pixels / meter.

        # Check if car or truck
        if params[11] != VehicleTypes.Truck:
            img = SIMPLE_CAR_SPRITE
        else:
            img = SIMPLE_TRUCK_SPRITE

        pyglet.sprite.Sprite.__init__(self, img, x=pos,
                                      y=LANE_WIDTH*lane + y_offset,
                                      batch=batch,
                                      group=group)

        self.scale = scale
        self.lane = lane
        self.y_offset = y_offset
        self.vel = vel
        self.params = params

    def set_color(self, new_vel):
        """
        Set the color for a vehicle based on how close it is to its desired
        speed.
        """
        # Stationary vehicles are green.
        if new_vel == 0:
            self.color = (0, 255, 0)
            return

        # Otherwise vehicles are blue when satisfied, red when unsatisfied.
        red_fact = int(1 - new_vel / self.params[1] * 255)
        blue_fact = 255 - red_fact
        self.color = (red_fact, 0, blue_fact)

    def update(self, new_pos, new_vel, new_lane, new_params):
        """
        Update the sprites location and color.
        """
        self.lane = new_lane
        self.x = new_pos * self.rescale
        self.y = (LANE_WIDTH - self.height) / 2 + self.y_offset + \
            LANE_WIDTH*self.lane
        self.params = new_params
        self.set_color(new_vel)


class SimAnimator(pyglet.window.Window):
    def __init__(self, logfp):
        # Load the simulation and data needed for init.
        self.sim_data = open(logfp, 'r')
        self.sim_name = str(self.sim_data.readline()).strip()
        self.track_length = int(self.sim_data.readline().strip())
        self.n_lanes = int(self.sim_data.readline().strip())

        # Set window properties
        self.rescale = float(WINDOW_WIDTH) / self.track_length

        # Relative with respect to 0, i.e. the bottom of the window.
        self.road_height = MARKER_LINE_HEIGHT + SPACER_HEIGHT + \
            TEXT_LINE_HEIGHT * (self.n_lanes + 1)
        window_height = self.road_height + LANE_WIDTH * self.n_lanes
        w_aspects = (WINDOW_WIDTH, window_height)
        title = 'Traffic simulation of log file - ' + logfp

        pyglet.window.Window.__init__(self, *w_aspects, caption=title)

        # Create a batch for performance and two groups to manage layering.
        self.main_batch = pyglet.graphics.Batch()
        self.background = pyglet.graphics.OrderedGroup(0)
        self.foreground = pyglet.graphics.OrderedGroup(1)

        # Load all data from the first time step. Need to do this before
        # initing road.
        self.init_simulation()

        # Set the road color and let it cover the entire width of the track
        self.road_bg = pyglet.sprite.Sprite(ROAD_BG, 0,
                                            self.road_height,
                                            batch=self.main_batch,
                                            group=self.background)
        self.road_bg.scale = float(self.road_height) / self.road_bg.height

        # Schedule the animation.
        self.schedule = pyglet.clock.schedule_interval(func=self.update,
                                                       interval=1/FPS)

    def init_simulation(self):
        """
        Create all objects / sprites needed for animation.
        """
        self.lines = []

        self.load_static_text()

        # Load traffic cams
        self.init_cams()

        # Load/init text, vehicles, lanes/road
        self.load_text()
        self.init_text()
        self.init_vehicles()
        self.init_lanes()

    def load_static_text(self):
        """
        Create all static labels.
        """
        self.sim_data.readline()

        self.static_labels = []

        # Read max_time, time_step and skip an empty line.
        time_max = round(float(self.sim_data.readline().strip()), 1)
        time_step = round(float(self.sim_data.readline().strip()), 1)
        n_vehs = int(self.sim_data.readline().strip())
        self.sim_data.readline()

        self.static_labels.extend([
                                   'Track length: ' + str(self.track_length) +
                                   ' m',
                                   'Max time: ' + str(time_max) + ' s',
                                   'Time step: ' + str(time_step) + ' s',
                                   'Amount of vehicles: ' + str(n_vehs),
                                   ])

    def init_cams(self):
        """
        Create all traffic cam lines.
        """
        line = self.sim_data.readline()
        self.cams = []

        # While the line is not empty
        while line.strip():
            (pos, max_vel) = read_tuple(line)
            x = pos * self.rescale
            self.lines.append(pt.Line((x, self.road_height),
                                      (x, self.height),
                                      stroke=LANE_STROKE,
                                      color=(1., 0., 0., 0.5)))
            self.cams.append(pyglet.text.Label(str(max_vel) + ' m/s',
                                               font_size=9,
                                               x=x,
                                               y=self.road_height - 5,
                                               anchor_x='center',
                                               anchor_y='top',
                                               color=(255, 0, 0, 255),
                                               batch=self.main_batch,
                                               group=self.foreground
                                               ))

            line = self.sim_data.readline()

    def load_text(self):
        """
        Load / compute the dynamic text each timestep from the LOG-file.
        """
        # Check if the animation is finished.
        first_line = self.sim_data.readline().strip()
        if first_line == 'SIMULATION SUCCESFUL':
            self.end_ani()

        self.labels = []

        time = first_line
        avg_vel = round(float(self.sim_data.readline().strip()), 1)
        min_vel = round(float(self.sim_data.readline().strip()), 1)
        max_vel = round(float(self.sim_data.readline().strip()), 1)

        self.labels.append([
                            'Time: ' + str(time) + ' s',
                            'Global:',
                            'Avg vel: ' + str(avg_vel) + ' m/s',
                            'Min vel: ' + str(min_vel) + ' m/s',
                            'Max vel: ' + str(max_vel) + ' m/s',
                            ])

        for i in xrange(self.n_lanes):
            avg_vel = self.read_property()
            min_vel = self.read_property()
            max_vel = self.read_property()
            self.labels.append([
                                '',
                                'Lane ' + str(i) + ':',
                                'Avg vel: ' + str(avg_vel) + ' m/s',
                                'Min vel: ' + str(min_vel) + ' m/s',
                                'Max vel: ' + str(max_vel) + ' m/s',
                                ])

    def init_text(self):
        """
        Create all text labels.
        """
        for i in xrange(len(self.static_labels)):
            width = TEXTBOX_X_OFFSET + 1.5*i*TEXTBOX_WIDTH
            height = TEXTBOX_Y_OFFSET
            pyglet.text.Label(self.static_labels[i],
                              font_size=10,
                              x=width, y=height,
                              anchor_x='left',
                              anchor_y='bottom',
                              batch=self.main_batch)

        self.text_labels = []
        for j in xrange(len(self.labels)):
            self.text_labels.append([])

            for i in xrange(len(self.labels[j])):
                width = TEXTBOX_X_OFFSET + i*TEXTBOX_WIDTH
                height = TEXTBOX_Y_OFFSET + (j+1)*TEXT_LINE_HEIGHT
                self.text_labels[j].append(
                        pyglet.text.Label(self.labels[j][i],
                                          font_size=10,
                                          x=width, y=height,
                                          anchor_x='left',
                                          anchor_y='bottom',
                                          batch=self.main_batch))

    def init_vehicles(self):
        """
        Create all VehicleSprite objects.
        """
        self.vehicles = {}

        line = self.sim_data.readline()

        # While the line is not empty
        while line.strip():
            veh_info = line.split('  ')
            pos = float(veh_info[0])
            vel = float(veh_info[1])
            veh_params = read_tuple(veh_info[3])
            veh_id = int(veh_info[4])

            # Create new vehicle
            new_veh = VehicleSprite(pos, self.road_height,
                                    vel, veh_params,
                                    scale=self.rescale,
                                    batch=self.main_batch,
                                    group=self.foreground)
            self.vehicles[veh_id] = new_veh

            line = self.sim_data.readline()

    def init_lanes(self):
        """
        Create all lane labels and the road background.
        """
        self.lane_label = []
        for i in xrange(self.n_lanes):
            lane_num = self.n_lanes - i - 1
            height = self.road_height + lane_num*LANE_WIDTH

            self.lines.append(pt.Line((0, height),
                                      (WINDOW_WIDTH, height),
                                      stroke=LANE_STROKE,
                                      color=(1., 1., 1., 1.)))
            self.lane_label.append(
                    pyglet.text.Label(str(lane_num),
                                      font_size=9,
                                      x=20,
                                      y=height + LANE_WIDTH / 2.0,
                                      anchor_x='left',
                                      anchor_y='center',
                                      batch=self.main_batch,
                                      group=self.foreground
                                      ))

    def read_property(self):
        """
        Read a single property from the LOG-file.
        """
        return round(float(self.sim_data.readline().strip().split('  ')[1]), 1)

    def update_text(self):
        """
        Update all text labels.
        """
        for j in xrange(len(self.labels)):
            for i in xrange(len(self.labels[j])):
                self.text_labels[j][i].text = self.labels[j][i]

    def update(self, interval):
        """
        Update the animation window.
        """
        # Load and update text labels.
        self.load_text()
        self.update_text()

        # Update the vehicles, reading info from the log.
        line = self.sim_data.readline().strip()

        # While the line is not empty, update vehicles
        while line:
            if line == 'SIMULATION SUCCESFUL':
                self.end_ani()
            veh_params = line.split('  ')

            new_pos = float(veh_params[0])
            new_vel = float(veh_params[1])
            new_lane = int(veh_params[2])
            new_params = read_tuple(veh_params[3])
            veh_id = int(veh_params[4])

            # Update vehicle
            veh = self.vehicles[veh_id]
            veh.update(new_pos, new_vel, new_lane, new_params)

            line = self.sim_data.readline().strip()

    def end_ani(self):
        """
        End the animation, do cleanup.
        """
        self.sim_data.close()
        self.close()
        sys.exit('Simulation completed succesfully.')

    def on_draw(self):
        """
        Draw all sprites and labels.
        """
        self.clear()
        self.main_batch.draw()

        for line in self.lines:
            line.render()


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Run a simulation.')
    parser.add_argument('anifile',
                        type=str,
                        help='filename of animition (LOG) file')
    parser.add_argument('FPS',
                        type=float,
                        const=45.0,
                        default=45.0,
                        nargs='?',
                        help='Frames per second for the animation')
    args = parser.parse_args()
    FPS = args.FPS
    anim = SimAnimator(args.anifile)
    pyglet.app.run()
