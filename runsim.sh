#!/bin/sh
SIMNAME='energy_usage_standard'
PRINTNAME='out_'$SIMNAME

# Backup
mkdir ./BAK/ -p
mv ./EXPERIMENTS/$SIMNAME ./BAK/

python simulationgen.py $SIMNAME
mkdir ./EXPERIMENTS/$SIMNAME/SIMS -p
mkdir ./EXPERIMENTS/$SIMNAME/LOGS -p
mv SIM_$SIMNAME* ./EXPERIMENTS/$SIMNAME/SIMS

for sim in ./EXPERIMENTS/$SIMNAME/SIMS/*; do
    python main.py $sim
done
mv LOG_$SIMNAME* ./EXPERIMENTS/$SIMNAME/LOGS

python plotting.py ./EXPERIMENTS/$SIMNAME/LOGS ./plotlog/$PRINTNAME
